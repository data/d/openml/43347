# OpenML dataset: Palmer-Penguins-Dataset-Alternative-Iris-Dataset

https://www.openml.org/d/43347

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Palmer Penguins Dataset
The goal of palmerpenguins is to provide a great dataset for data exploration  visualization, as an alternative to iris.
About the data
Data were collected and made available by Dr. Kristen Gorman and the Palmer Station, Antarctica LTER, a member of the Long Term Ecological Research Network.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43347) of an [OpenML dataset](https://www.openml.org/d/43347). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43347/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43347/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43347/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

